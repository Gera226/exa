package com.example.exa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.exa.database.productoDatos;
import com.example.exa.database.productos;
import com.example.exa.database.productoDatos;
import com.example.exa.database.productosDbHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;

    private productoDatos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnEditar = findViewById(R.id.btnEditar);
        db = new productoDatos(this);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if ( txtCodigo.getText().toString().equals("")|| txtNombre.getText().toString().equals("") ||
                        txtPrecio.getText().toString().equals("") || txtMarca.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "No puedes dejar los campos vacios",
                            Toast.LENGTH_SHORT).show();

                }else {

                    productos productos = new productos();
                    productos.setCodigo(Integer.parseInt(getText(txtCodigo)));
                    productos.setNombreProducto(getText(txtNombre));
                    productos.setPreacio(Float.parseFloat(getText(txtPrecio)));
                    productos.setMarca(getText(txtMarca));
                    productos.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
                    db.insertProducto(productos);
                    Toast.makeText(MainActivity.this, "Se agrego correctamente", Toast.LENGTH_SHORT).show();


                }
                limpiar();
                db.close();


            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Producto.class);
                startActivity(intent);
            }
        });
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }


    private void btnLimpiarAction(View view) {
        limpiar();
    }

    private void limpiar() {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
    }




    private String getText(EditText txt) {
        return txt.getText().toString();
    }
}