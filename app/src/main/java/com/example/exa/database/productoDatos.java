package com.example.exa.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.exa.MainActivity;

import java.util.ArrayList;

public class productoDatos {
    private Context context;
    private productosDbHelper helper;
    private SQLiteDatabase db;

    private String[] columns =
            {DefinirTabla.ID, DefinirTabla.NOMBRE_PRODUCTO, DefinirTabla.MARCA,
                    DefinirTabla.PRECIO, DefinirTabla.PERECEDERO, DefinirTabla.CODIGO};

    public productoDatos(Context context)
    {
        this.context = context;
        this.helper = new productosDbHelper(context);

    }

    public void openDatabase()
    {
        db = helper.getWritableDatabase();
    }

    public long insertProducto(productos producto)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE_PRODUCTO, producto.getNombreProducto());
        values.put(DefinirTabla.MARCA, producto.getMarca());
        values.put(DefinirTabla.PRECIO, producto.getPreacio());
        values.put(DefinirTabla.PERECEDERO, producto.isPerecedero() ? 1 : 0);

        return db.insert(DefinirTabla.TABLE_PRODUCTO, null, values);

    }

    public long updateProducto(productos producto)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE_PRODUCTO, producto.getNombreProducto());
        values.put(DefinirTabla.MARCA, producto.getMarca());
        values.put(DefinirTabla.PRECIO, producto.getPreacio());
        values.put(DefinirTabla.PERECEDERO, producto.isPerecedero() ? 1 : 0);
        String where = DefinirTabla.ID + "=" + producto.getId();
        return db.update(DefinirTabla.TABLE_PRODUCTO, values, where, null);
    }

    public long deleteProducto(long id)
    {
        String where = DefinirTabla.ID + "=" + id;
        return db.delete(DefinirTabla.TABLE_PRODUCTO, where, null);

    }

    private productos readProducto(Cursor cursor)
    {
        productos producto = new productos();
        producto.setId(cursor.getInt(0));
        producto.setNombreProducto(cursor.getString(1));
        producto.setMarca(cursor.getString(2));
        producto.setPreacio(cursor.getFloat(3));
        producto.setPerecedero(cursor.getInt(4) == 1);
        producto.setCodigo(cursor.getInt(5));
        return producto;

    }

    public productos getProducto(long id)
    {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.TABLE_PRODUCTO,columns,
                DefinirTabla.ID + " = ?" , new String[]{String.valueOf(id)},null,
                null, null);
        if(cursor.moveToFirst())
        {
            productos contacto =this.readProducto(cursor);
            cursor.close();
            return contacto;
        }
        else
            return null;

    }

    public ArrayList<productos> allContactos()
    {
        ArrayList<productos> productos = new ArrayList<>();
        Cursor cursor = db.query(DefinirTabla.TABLE_PRODUCTO, columns, null,
                null, null, null,null);
        if(cursor!= null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                productos.add(readProducto(cursor));
                cursor.moveToNext();
            }
            cursor.close();

        }


        return productos;

    }

    public void close()
    {
        helper.close();
    }
}
