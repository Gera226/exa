package com.example.exa.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class productosDbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 3;
    private static final String NAME_db = "sistema.db";
    private static String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTabla.TABLE_PRODUCTO;
    public productosDbHelper(Context context)
    {
        super(context, NAME_db, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);

    }

    private String createTable()
    {
        Sql tableContacto = new Sql(DefinirTabla.TABLE_PRODUCTO, DefinirTabla.ID);
        tableContacto.addColumn(DefinirTabla.NOMBRE_PRODUCTO, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.MARCA, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.PRECIO, tableContacto.TYPE_REAL);
        tableContacto.addColumn(DefinirTabla.PERECEDERO, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.CODIGO, tableContacto.TYPE_INTEGER);
        return tableContacto.getQuery();
    }
}
