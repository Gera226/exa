package com.example.exa.database;

import java.io.Serializable;

public class productos implements Serializable {
    private long id;
    private String nombreProducto;
    private String marca;
    private float preacio;
    private boolean perecedero;
    private int codigo;


    public productos()
    {

    }

    public productos(long id,int codigo, String nombreProducto, String marca, float preacio, boolean perecedero) {
        this.id = id;
        this.codigo = codigo;
        this.nombreProducto = nombreProducto;
        this.marca = marca;
        this.preacio = preacio;
        this.perecedero = perecedero;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPreacio() {
        return preacio;
    }

    public void setPreacio(float preacio) {
        this.preacio = preacio;
    }

    public boolean isPerecedero() {
        return perecedero;
    }

    public void setPerecedero(boolean perecedero) {
        this.perecedero = perecedero;
    }
}
