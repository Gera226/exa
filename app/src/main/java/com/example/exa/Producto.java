package com.example.exa;

import android.os.Bundle;

import com.example.exa.database.productoDatos;
import com.example.exa.database.productos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Producto extends AppCompatActivity {

    private EditText txtCodigo;
    private Button btnBuscar;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnBorrar;
    private Button btnEditar;
    private Button btnCerrar;
    private productoDatos db;
    private productos saveObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnEditar = findViewById(R.id.btnEditar);
        btnCerrar = findViewById(R.id.btnCerrar);
        db = new productoDatos(this);

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if ( txtCodigo.getText().toString().equals("")|| txtNombre.getText().toString().equals("") ||
                        txtPrecio.getText().toString().equals("") || txtMarca.getText().toString().equals("")) {
                    Toast.makeText(Producto.this, "No puedes dejar los campos vacios",
                            Toast.LENGTH_SHORT).show();

                }else {
                    productos productos = new productos();
                    //producto.setCodigo(Integer.parseInt(getText(txtCodigo)));
                    productos.setId(saveObject.getId());
                    productos.setNombreProducto(getText(txtNombre));
                    productos.setPreacio(Float.parseFloat(getText(txtPrecio)));
                    productos.setMarca(getText(txtMarca));
                    productos.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
                    db.updateProducto(productos);
                    Toast.makeText(Producto.this, "Modificado correctamente",
                            Toast.LENGTH_SHORT).show();
                    limpiar();
                    db.close();
                }
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                productos productos = db.getProducto(Long.parseLong(getText(txtCodigo)));
                if(productos == null)
                {
                    Toast.makeText(Producto.this, "No se encontro producto", Toast.LENGTH_LONG).show();
                }
                else
                {
                    saveObject = productos;
                    txtMarca.setText(saveObject.getMarca());
                    txtNombre.setText(saveObject.getNombreProducto());
                    txtPrecio.setText(String.valueOf(saveObject.getPreacio()));
                    rbgPerecedero.check(saveObject.isPerecedero() ? R.id.rbPerecedero : R.id.rbNoPerecedero);
                }
                db.close();
            }
        });
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                db.deleteProducto(Long.parseLong(getText(txtCodigo)));
                Toast.makeText(Producto.this, "Se elimino el producto", Toast.LENGTH_LONG).show();
                db.close();
                limpiar();
            }
        });
    }



    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        saveObject = null;

    }



    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}


